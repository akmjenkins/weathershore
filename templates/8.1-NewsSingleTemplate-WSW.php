<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe" data-controls="true" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/bin/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<h1 class="title">News Single Template</h1>
							
							<p>Sub Title</p>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw">
			<a href="#">The Latest</a>
			<a href="#">News Single Template</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<article>
	
		<section>
			<div class="sw">
				<div class="main-body">
				
					<div class="time">
						
						<time datetime="2014-06-15">
							<span class="day">15</span>
							
							<span class="month-year">
								<span class="month">Jun</span>
								<span class="year">2014</span>
							</span><!-- .month-year -->
							
						</time>
						
					</div><!-- .time -->
				
					<div class="content">
						<div class="article-body">
						
							<p class="excerpt">Vivamus elementum elementum lacus. Mauris auctor tempus justo, ac tincidunt turpis tempor pharetra. Aenean facilisis diam mi, vitae pellentesque arcu vestibulum id. Fusce fermentum, diam ut dapibus posuere, purus tortor vestibulum tortor, vel rhoncus lacus metus nec nisl. Maecenas congue imperdiet pretium. Sed interdum tempus sem, ut varius arcu aliquam a.</p>

							<p>Maecenas venenatis, elit eget posuere luctus, nunc sem malesuada purus, vel consectetur quam purus quis est. Nulla eget ipsum porta, consequat augue id, porttitor augue. Suspendisse euismod, diam at lacinia feugiat, mauris libero lacinia felis, eu tincidunt libero nulla convallis est. Nunc nec turpis a libero accumsan fringilla. Donec porta mi in massa ultrices venenatis. Ut non eleifend sem. Integer nibh mauris, viverra ac ante nec, molestie bibendum tellus. Donec egestas quis lorem sit amet volutpat.</p>

							<p>Fusce magna risus, elementum ut commodo tempus, egestas at nisi. Cras consequat cursus erat ac tempus. Morbi tempor sit amet sapien ac posuere. Etiam at leo eleifend, malesuada enim a, volutpat eros. Nunc fermentum condimentum ultricies. Aliquam nibh arcu, suscipit a tempus nec, facilisis non metus. Nam id facilisis dolor. Duis in massa rhoncus, accumsan mauris id, vulputate nunc. Donec sagittis leo lorem, ut cursus felis convallis non.</p>

							<p>Etiam dignissim ex quis lectus sollicitudin bibendum. In ultrices ultrices arcu, vitae auctor neque rhoncus sit amet. Vivamus lacinia urna mauris, eget volutpat erat tempus vel.Brazil, Japan, Argentina, France, Holland and Singapore.</p>
						
						</div><!-- .article-body -->
					</div><!-- .content -->
					
					<aside class="sidebar">
					
						<?php include('inc/i-archives.php'); ?>
					
					</aside><!-- .sidebar -->
					
				</div><!-- .main-body -->
			</div><!-- .sw -->
		</section>
	
	</article>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>