<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe" data-controls="true" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/bin/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<h1 class="title">Why Us</h1>
							
							<p>Reliable. Quality. Made Right Here.</p>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">

	<div>

		<a href="#" class="overview-block bounce">
			<div class="sw">
			
				<div class="content">
				
					<div class="section-title">
						<h2>Our Story</h2>
						<span class="subtitle">A storied past and an even brighter future</span>
					</div><!-- .section-title -->
					
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan 
					et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
					
					<span class="button yellow">Read More</span>
				
				</div><!-- .content -->
				
				<div class="img-wrap">
					<div class="img lazybg" data-src="../assets/bin/images/temp/overview/ov1.jpg"></div>
				</div><!-- .img-wrap -->
			
			</div><!-- .sw -->
		</a><!-- .overview-block -->
		
		<a href="#" class="overview-block bounce">
			<div class="sw">
			
				<div class="content">
				
					<div class="section-title">
						<h2>Our Commitment</h2>
						<span class="subtitle">Quality Products. Quality Service. </span>
					</div><!-- .section-title -->
					
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan 
					et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
					
					<span class="button yellow">Read More</span>
				
				</div><!-- .content -->
				
				<div class="img-wrap">
					<div class="img lazybg" data-src="../assets/bin/images/temp/overview/ov2.jpg"></div>
				</div><!-- .img-wrap -->
			
			</div><!-- .sw -->
		</a><!-- .overview-block -->
		
		<a href="#" class="overview-block bounce">
			<div class="sw">
			
				<div class="content">
				
					<div class="section-title">
						<h2>Our Customer Experience</h2>
						<span class="subtitle">By your side from design to installation.</span>
					</div><!-- .section-title -->
					
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan 
					et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
					
					<span class="button yellow">Read More</span>
				
				</div><!-- .content -->
				
				<div class="img-wrap">
					<div class="img lazybg" data-src="../assets/bin/images/temp/overview/ov3.jpg"></div>
				</div><!-- .img-wrap -->
			
			</div><!-- .sw -->
		</a><!-- .overview-block -->

		<a href="#" class="overview-block bounce">
			<div class="sw">
			
				<div class="content">
				
					<div class="section-title">
						<h2>Contact Us</h2>
						<span class="subtitle">We are here to answer any question.</span>
					</div><!-- .section-title -->
					
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					
					<span class="button yellow">Read More</span>
				
				</div><!-- .content -->
				
				<div class="img-wrap">
					<div class="img lazybg" data-src="../assets/bin/images/temp/overview/ov4.jpg"></div>
				</div><!-- .img-wrap -->
			
			</div><!-- .sw -->
		</a><!-- .overview-block -->

	</div>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>