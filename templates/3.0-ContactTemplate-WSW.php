<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe" data-controls="true" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/bin/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<h1 class="title">Contact</h1>
							
							<p>Sub TItle</p>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw">
			<a href="#">Contact</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<section>
		<div class="sw">
			<div class="main-body">
				<div class="content">
					<div class="article-body">
					
						<p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
						felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
						felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
					
					</div><!-- .article-body -->
				</div><!-- .content -->
			</div>
		</div><!-- .sw -->
	</section>
	
	<section class="grey-bg">
		<div class="sw">
		
			<p>Fill out the form below to contact us</p>
		
			<div class="grid pad40">
			
				<div class="col col-2 sm-col-1">
					<div class="item">
						
						<form action="/" method="post" class="body-form">
							<fieldset>
								
								<input type="text" name="name" placeholder="Full Name">
								<input type="email" name="email" placeholder="Email Address">
								<input type="tel" pattern="\d+" placeholder="Phone">
								<textarea name="message" cols="30" rows="10" placeholder="Message..."></textarea>
								
								<button class="button yellow big" type="submit">Submit</button>
								
							</fieldset>
						</form><!-- .body-form -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-2 sm-col-1">
					<div class="item">
					
						<h2>Location</h2>
					
						<div class="grid">
						
							<div class="col col-2-5 xs-col-1">
								<div class="item">
								
									<address>
										Weather Shore Windows Inc. <br />
										77 Blackmarsh Road <br />
										St. John's, NL A1E 156
									</address>
									
								</div><!-- .item -->
							</div><!-- .col -->
							
							<div class="col col-2 xs-col-1">
								<div class="item">
								
									<span class="block">
										Tel: (709) 753-7640
									</span>
									<span class="block">
										Fax: (709) 753-6264
									</span>
								
								</div><!-- .item -->
							</div><!-- .col -->
							
							<div class="col col-2-5 sm-col-1">
								<div class="item">
									
									<h6 class="serif">Store Hours</h6>
									<span class="block">Monday - Friday 9am &ndash; 9pm</span>
									<span class="block">Saturday &ndash; 9am &ndash; 5pm</span>
									<span class="block">Sunday &ndash; Closed</span>
									
								</div><!-- .item -->
							</div><!-- .col -->
							
							<div class="col col-3-5 sm-col-1">
								<div class="item">
									
									<div class="video-container full">
										<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d10771.575742922703!2d-52.731886!3d47.550393!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa05d70a058bed9eb!2sWeather+Shore+Windows+Inc!5e0!3m2!1sen!2sca!4v1417183938899"frameborder="0" style="border:0"></iframe>
									</div><!-- .responsive-video -->
									
								</div><!-- .item -->
							</div><!-- .col -->
							
						</div><!-- .grid -->
						
						
						
					
					</div><!-- .item -->
				</div><!-- .col -->
				
			</div><!-- .grid -->
			
		</div><!-- .sw -->
	</section><!-- .grey-bg -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>