<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe" data-controls="true" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/bin/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<h1 class="title">Our Customer Experience</h1>
							
							<p>A storied past and an even brighter future</p>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw">
			<a href="#">Why Us</a>
			<a href="#">Our Story</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<section>
		<div class="sw">
			<div class="main-body">
				<div class="content">
					<div class="article-body">
					
						<p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
						felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
						felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
					
					</div><!-- .article-body -->
				</div><!-- .content -->
			</div>
		</div><!-- .sw -->
	</section>
	
	<div>
		<div class="overview-block">
			<div class="sw">
			
				<div class="content">
				
					<div class="section-title">
						<h2>Meet with freindly staff</h2>
						<span class="subtitle">A storied past and an even brighter future</span>
					</div><!-- .section-title -->
					
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan 
					et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
				
				</div><!-- .content -->
				
				<div class="img-wrap">
					<div class="img lazybg" data-src="../assets/bin/images/temp/overview/ov8.jpg"></div>
				</div><!-- .img-wrap -->
			
			</div><!-- .sw -->
		</div><!-- .overview-block -->
		
		<div class="overview-block">
			<div class="sw">
			
				<div class="content">
				
					<div class="section-title">
						<h2>Determine your needs</h2>
						<span class="subtitle">A storied past and an even brighter future</span>
					</div><!-- .section-title -->
					
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan 
					et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
				
				</div><!-- .content -->
				
				<div class="img-wrap">
					<div class="img lazybg" data-src="../assets/bin/images/temp/overview/ov9.jpg"></div>
				</div><!-- .img-wrap -->
			
			</div><!-- .sw -->
		</div><!-- .overview-block -->
		
		<div class="overview-block">
			<div class="sw">
			
				<div class="content">
				
					<div class="section-title">
						<h2>Walk through design process</h2>
						<span class="subtitle">A storied past and an even brighter future</span>
					</div><!-- .section-title -->
					
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan 
					et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
				
				</div><!-- .content -->
				
				<div class="img-wrap">
					<div class="img lazybg" data-src="../assets/bin/images/temp/overview/ov10.jpg"></div>
				</div><!-- .img-wrap -->
			
			</div><!-- .sw -->
		</div><!-- .overview-block -->
	</div>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>