<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe" data-controls="true" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/bin/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<h1 class="title">Doors</h1>
							
							<p>By your side from design to installation</p>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">

	<div class="breadcrumbs">
		<div class="sw">
			<a href="#">Contact</a>
		</div><!-- .sw -->
	</div><!-- .breadcrumbs -->
	
	<section>
		<div class="sw">
			<div class="main-body">
			
				<div class="content">
					<div class="article-body">
					
						<p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
						felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, 
						felis tellus mollis orci, sed rhoncus sapien nunc eget odio.</p>
					
					</div><!-- .article-body -->
				</div><!-- .content -->
				
				<aside class="sidebar">
					
					<a class="callout dark-bg" href="#" style="background-image: url(../assets/bin/images/temp/request-a-quote.jpg);">
					
						<p>
							Get the effiiency, elegance and dependability you deserve.
						</p>
						
						<span class="big yellow button">Request a Quote</span>
					</a><!-- .callout -->
					
				</aside><!-- .sidebar -->
				
			</div>
		</div><!-- .sw -->
	</section>
	
	<section class="split-screen dark-bg center collapse-1000">
		<div class="sw">
			<div class="grid ss eqh collapse-1000">
				<div class="col col-2">
					<a class="item" href="#">
					
						<div class="section-title">
							<h2>Entry Doors</h2>
							<span class="subtitle">Sub title</span>
						</div><!-- .section-title -->
						
						<div class="section-excerpt">
							<p>Sed congue volutpat quam, a euismod orci varius a. Morbi quis sapien ex. Proin vestibulum vel arcu accumsan porta. Etiam leo risus, porttitor a fermentum et, aliquet quis dolor.</p>
						</div><!-- .section-excerpt -->
						
						<div class="center">
							<span class="button big">Read More</span>
						</div><!-- .center -->
						
					</a><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2">
					<a class="item" href="#">
						
							<div class="section-title">
								<h2>Patio Doors</h2>
								<span class="subtitle">Sub title</span>

							</div><!-- .section-title -->
							
							<div class="section-excerpt">
								<p>Sed congue volutpat quam, a euismod orci varius</p>
							</div><!-- .section-excerpt -->
							
							<div class="center">
								<span class="button big">Read More</span>
							</div><!-- .center -->
							
					</a><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		</div><!-- .sw -->
	</section><!-- .split-screen -->
	
	<section class="split-screen dark-bg center collapse-1000">
		<div class="sw">
			<div class="grid ss eqh collapse-1000">
				<div class="col col-2">
					<a class="item" href="#">
					
						<div class="section-title">
							<h2>Features &amp; Options</h2>
							<span class="subtitle">Sub title</span>
						</div><!-- .section-title -->
						
						<div class="section-excerpt">
							<p>Sed congue volutpat quam, a euismod orci varius a. Morbi quis sapien ex. Proin vestibulum</p>
						</div><!-- .section-excerpt -->
						
						<div class="center">
							<span class="button big">Read More</span>
						</div><!-- .center -->
						
					</a><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2">
					<a class="item" href="#">
						
							<div class="section-title">
								<h2>Doot Installation</h2>
								<span class="subtitle">Sub title</span>

							</div><!-- .section-title -->
							
							<div class="section-excerpt">
								<p>Sed congue volutpat quam, a euismod orci varius a. Morbi quis sapien ex. Proin vestibulum vel arcu accumsan porta. Etiam leo risus, porttitor a fermentum et, aliquet quis dolor.</p>
							</div><!-- .section-excerpt -->
							
							<div class="center">
								<span class="button big">Read More</span>
							</div><!-- .center -->
							
					</a><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		</div><!-- .sw -->
	</section><!-- .split-screen -->

	
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>