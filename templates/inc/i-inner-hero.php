<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe" data-controls="true" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/bin/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<span class="title h1-style">Why Us</span>
							
							<p>
								Reliable. Quality. Made Right Here.
							</p>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->