<div class="build-price-window">
	<div class="bp-wrap">
		<div class="title-bar">
			<div class="sw">
				<div class="title-wrap">
					<h2 class="title">Build &amp; Price</h2>
					<span class="type">Awning Window</span>
					
					<button class="t-fa fa-close close">Close</button>
				</div><!-- .title-wrap -->
			</div><!-- .sw -->
			
			<div class="bp-nav green-bg">
				<div class="sw">
				
					<div class="selector with-arrow">
						<select class="tab-controller">
							<option>Vent Style</option>
							<option>Window Size</option>
							<option>Grill Style</option>
							<option>Type of Glass</option>
							<option>Trim Colour</option>
							<option>Other Options</option>
							<option>Quantity</option>
							<option>Summary</option>
						</select>
						<span class="value">&nbsp;</span>
					</div><!-- .selector -->
				
					<button class="selected">Vent Style</button>
					<button>Window Size</button>
					<button>Grill Style</button>
					<button>Type of Glass</button>
					<button>Trim Colour</button>
					<button>Other Options</button>
					<button>Quantity</button>
					<button>Summary</button>
				
				</div><!-- .sw -->
			</div><!-- .bp-nav -->
			
		</div><!-- .title-bar -->
			
		<div class="bp-content with-loading-indicator">
		
			<!-- this area is populated by the HTML returned from the URL specified by the data-builderurl attribute -->
		
		</div><!-- .bp-content -->
	</div><!-- .bp-wrap -->
</div><!-- .build-price -->