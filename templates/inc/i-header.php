<?php

	//you can remove these, I only included it so I could simulate WordPress conditionals while I was building the template
	function is_home() {
		global $bodyclass;
		return preg_match('/home/',$bodyclass);
	}

	function is_404() {
		global $bodyclass;
		return preg_match('/error404/',$bodyclass);
	}
?>
<!doctype html>
<html lang="en">

	<head>
		<title>Weather Shore Windows</title>
		<meta charset="utf-8">
		
		<!-- typekit: Grafolita Script accessible only via 127.0.0.1 -->
		<script src="//use.typekit.net/gns1hzo.js"></script>
		<script>try{Typekit.load();}catch(e){}</script>
		
		<!-- modernizr - minus the shiv, still useful for adding tests -->
		<script src="../assets/bin/js/lib/modernizr/modernizr.js"></script>
		
		<!-- jQuery -->
		<script src="../assets/bin/lib/js/jquery/jquery.min.js"></script>	
		
		<!-- tooltipster -->
		<script src="../assets/bin/lib/js/tooltipster/jquery.tooltipster.min.js"></script>	
		<link rel="stylesheet" href="../assets/bin/lib/css/tooltipster/tooltipster.css">
		
		<!-- magnific popup -->
		<script src="../assets/bin/lib/js/magnific-popup/jquery.magnific-popup.min.js"></script>
		<link rel="stylesheet" href="../assets/bin/lib/css/magnific-popup/magnific-popup.css">
		
		<!-- favicons -->
		<link rel="icon" type="image/x-icon"  href="../assets/bin/images/favicons/favicon.ico">
		<link rel="icon" type="image/png"  href="../assets/bin/images/favicons/favicon-32.png">
		<link rel="icon" href="../assets/bin/images/favicons/favicon-32.png" sizes="32x32">
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="../assets/bin/images/favicons/favicon-152.png">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/bin/images/favicons/favicon-144.png">
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="../assets/bin/images/favicons/favicon-120.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/bin/images/favicons/favicon-114.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/bin/images/favicons/favicon-144.png">
		<link rel="apple-touch-icon-precomposed" href="../assets/bin/images/favicons/favicon-114.png">	
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="../assets/bin/images/favicons/favicon-144.png">
		
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0">
		
		<!-- 
			place our CSS after all the other stylesheets so that we can more easily override their styles
			The Stylesheet has the bower-defined version of font-awesome built into it
		-->
		<link rel="stylesheet" href="../assets/bin/css/style.css?<?php echo time(); ?>">		
		
	</head>
	<body class="<?php echo $bodyclass; ?>">
		<button class="toggle-nav">Mobile Nav</button>

		<?php include('i-nav.php'); ?>
	
		<div class="page-wrapper">	
			<header>
				<div class="sw">
					
					<a href="#" class="header-logo">
						<span>
							<img src="../assets/bin/images/weather-shore-windows-white-bg.svg" alt="Weather Shore Windows Logo">
						</span>
					</a>
					
				</div><!-- .sw -->
			</header>
