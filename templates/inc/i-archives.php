<div class="archives-mod">
	<h4 class="title">Archives</h4>
	
	<div class="accordion">
		<div class="accordion-item">
			<div class="accordion-item-handle">
				2014 (5)
			</div><!-- .accordion-item-handle -->
			<div class="accordion-item-content">
				<ul>
					<li class="selected"><a href="#">October (3)</a></li>
					<li><a href="#">September (2)</a></li>
				</ul>
			</div><!-- .accordion-item-content -->
		</div><!-- .accordion-item -->
		<div class="accordion-item">
			<div class="accordion-item-handle">
				2013 (10)
			</div><!-- .accordion-item-handle -->
			<div class="accordion-item-content">
				<ul>
					<li><a href="#">October (3)</a></li>
					<li><a href="#">September (2)</a></li>
					<li><a href="#">September (2)</a></li>
					<li><a href="#">September (2)</a></li>
				</ul>
			</div><!-- .accordion-item-content -->
		</div><!-- .accordion-item -->
		<div class="accordion-item">
			<div class="accordion-item-handle">
				2012 (15)
			</div><!-- .accordion-item-handle -->
			<div class="accordion-item-content">
				<ul>
					<li><a href="#">October (3)</a></li>
					<li><a href="#">October (3)</a></li>
					<li><a href="#">October (3)</a></li>
					<li><a href="#">October (3)</a></li>
					<li><a href="#">September (2)</a></li>
				</ul>
			</div><!-- .accordion-item-content -->
		</div><!-- .accordion-item -->
	</div><!-- .accordion -->
	
</div><!-- .archives -->
