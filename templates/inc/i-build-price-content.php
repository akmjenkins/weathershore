			<div>
				<div class="sw">				
				
					<div class="lazybg with-img">
						<img src="../assets/bin/images/temp/windows/build-price.jpg" alt="awning window"/>
					</div><!-- .img -->
					
					<div class="swiper-wrapper">
						<div class="swipe">
							<div class="swipe-wrap">
							
								<div>
									
									<h2>Please select your vent style</h2>
									<p>Vent style is Cras quis lacus interdum nisl ultricies rhoncus ut et est. Cras libero risus, viverra eu nulla ut, ornare gravida enim. 
									Sed aliquet ex eu mauris fringilla, feugiat viverra urna vestibulum.</p>
									
									<div class="options">
									
										<div class="option selected">
											<span class="b"><img src="../assets/bin/images/temp/windows/vent-style-1.png" alt="vent style 1"></span>
										</div><!-- .option -->
										
										<div class="option">
											<span class="b"><img src="../assets/bin/images/temp/windows/vent-style-2.png" alt="vent style 2"></span>
										</div><!-- .option -->
										
									</div><!-- .options -->
									<button class="next button" type="button">Next</button>
									
								</div>
								
								<div>
									
									<h2>Please select the size of your window</h2>
									<p>To set the window size,  cras quis lacus interdum nisl ultricies rhoncus ut et est. Cras libero risus, viverra eu nulla ut, ornare gravida enim. 
									Sed aliquet ex eu mauris fringilla, feugiat viverra urna vestibulum.</p>
									
									<br />
									
									<div class="body-form ib">
										<input type="text" name="width" placeholder="Width" class="ib">
										<input type="text" name="height" placeholder="Height" class="ib">
										<button class="next button" type="button">Next</button>
									</div><!-- .body-form -->
									
									
								</div>
								
								<div>
									
									<h2>Please select your type of grill</h2>
									<p>Grill style is  cras quis lacus interdum nisl ultricies rhoncus ut et est. Cras libero risus, viverra eu nulla ut, ornare gravida enim. 
									Sed aliquet ex eu mauris fringilla, feugiat viverra urna vestibulum.</p>
									
									<div class="options">
									
										<div class="option selected">
											<span class="b"><img src="../assets/bin/images/temp/windows/grill-style-1.png" alt="grill style 1"></span>
										</div><!-- .option -->
										
										<div class="option">
											<span class="b"><img src="../assets/bin/images/temp/windows/grill-style-2.png" alt="grill style 2"></span>	
										</div><!-- .option -->
										
										<div class="option">
											<button><img src="../assets/bin/images/temp/windows/grill-style-3.png" alt="grill style 3"></button>	
										</div><!-- .option -->
										
									</div><!-- .options -->
									<button class="next button" type="button">Next</button>
									
								</div>
								
								<div>
								
									<h2>Please select your type of glass</h2>
									<p>There are two style of glass to choose from.  Cras quis lacus interdum nisl ultricies rhoncus ut et est. Cras libero risus, viverra eu nulla 
									ut, ornare gravida enim. Sed aliquet ex eu mauris fringilla, feugiat viverra urna vestibulum.</p>
									
									<div class="options equal">
									
										<div class="option selected">
											<span class="b"><span>Clear</span></span>
										</div><!-- .option -->
										
										<div class="option">
											<span class="b">
												<i class="fa fa-question-circle tooltipster" data-selector=".tooltip-selectors .tooltip-1"></i>
												<span>Low-E Argon</span>
											</span>	
										</div><!-- .option -->
										
									</div><!-- .options -->
									
									<button class="next button" type="button">Next</button>
								
								</div>
								
								<div>
								
									<h2>Please select your trim colour</h2>
									<p>Weather Shore offers great colour options.  Cras quis lacus interdum nisl ultricies rhoncus ut et est. Cras libero risus, viverra eu nulla ut, ornare gravida enim. 
									Sed aliquet ex eu mauris fringilla, feugiat viverra urna vestibulum.</p>
									
									<div class="options colors">
										<div class="option selected">
											<span class="b ar" data-ar="100" style="background-color: #fff !important;"></span>
										</div>
										<div class="option">
											<span class="b ar" data-ar="100" style="background-color: #d3d3d3 !important;"></span>
										</div>
										<div class="option">
											<span class="b ar" data-ar="100" style="background-color: #dedcbe !important;"></span>
										</div>
										<div class="option">
											<span class="b ar" data-ar="100" style="background-color: #aca371 !important;"></span>
										</div>
										<div class="option">
											<span class="b ar" data-ar="100" style="background-color: #e8e192 !important;"></span>
										</div>
									</div>
									<button class="next button" type="button">Next</button>
								
								</div>
								
								<div>
								
									<h2>Other Options</h2>
									<p>To include these options, please select them. Leave them unselected not to include them.</p>
									
									<div class="options equal">
									
										<div class="option selected">
											<span class="b">
												<i class="fa fa-question-circle tooltipster" data-selector=".tooltip-selectors .tooltip-2"></i>
												<span>Brickmould</span>
											</span>
										</div><!-- .option -->
										
										<div class="option">
											<span class="b">
												<i class="fa fa-question-circle tooltipster" data-selector=".tooltip-selectors .tooltip-3"></i>
												<span>Drywall Return</span>
											</span>
										</div><!-- .option -->
										
										<div class="option">
											<span class="b">
												<i class="fa fa-question-circle tooltipster" data-selector=".tooltip-selectors .tooltip-4"></i>
												<span>Check for Egress</span>
											</span>
										</div><!-- .option -->
										
									</div><!-- .options -->
									<button class="next button" type="button">Next</button>
								
								</div>
								
								<div>
								
									<h2>Please select the quantity of windows</h2>
									<p>To set the window size,  cras quis lacus interdum nisl ultricies rhoncus ut et est. Cras libero risus, viverra eu nulla ut, ornare gravida enim. 
									Sed aliquet ex eu mauris fringilla, feugiat viverra urna vestibulum.</p>
									
									<br />
									
									<div class="body-form ib">
										<input type="text" name="qty" placeholder="Width">
										<button class="next button" type="button">Next</button>
									</div><!-- .body-form -->
								
								</div>
								
								<div>
								
									<h2>Your build summary.</h2>
									<p>Grill style is  cras quis lacus interdum nisl ultricies rhoncus ut et est. Cras libero risus, viverra eu nulla ut, ornare gravida enim. 
									Sed aliquet ex eu mauris fringilla, feugiat viverra urna vestibulum.</p>
									
									<div class="grid">
									
										<div class="col-2 col sm-col-1">
											<div class="item">
												
												<div class="rows">
													<div class="row">
														<span class="l">Window Type:</span>
														<span class="r">Awning</span>
													</div><!-- .row -->
													
													<div class="row">
														<span class="l">Vent Type:</span>
														<span class="r">Selected Vent Type</span>
													</div><!-- .row -->
													
													<div class="row">
														<span class="l">Size:</span>
														<span class="r">5 x 5</span>
													</div><!-- .row -->
													
													<div class="row">
														<span class="l">Grill Style:</span>
														<span class="r">Colonial</span>
													</div><!-- .row -->
													
													<div class="row">
														<span class="l">Glass:</span>
														<span class="r">Low E</span>
													</div><!-- .row -->
													
													<div class="row">
														<span class="l">Trim Colour:</span>
														<span class="r">Beige</span>
													</div><!-- .row -->
													
													<div class="row">
														<span class="l">Brickmuld:</span>
														<span class="r">No</span>
													</div><!-- .row -->
													
													<div class="row">
														<span class="l">Drywall Return:</span>
														<span class="r">No</span>
													</div><!-- .row -->
													
													<div class="row">
														<span class="l">Egress:</span>
														<span class="r">Yes</span>
													</div><!-- .row -->
													
													<div class="row">
														<span class="l">Quantity:</span>
														<span class="r">5</span>
													</div><!-- .row -->
													
												</div><!-- .rows -->
												
											</div><!-- .item -->
										</div><!-- .col -->
										
										<div class="col-2 col sm-col-1">
											<div class="item">
											
												<form action="" class="body-form" novalidate>
													<fieldset>
														<input type="text" name="name" placeholder="Full Name">
														<input type="email" name="email" placeholder="Email Address">
														<input type="tel" pattern="\d+" placeholder="Phone">
														
														<button class="button green big" type="submit">Submit</button>
													</fieldset>
												</form>
											
											</div><!-- .item -->
										</div><!-- .col -->
										
									</div><!-- .grid -->
								
								</div>
								
							</div><!-- .swipe-wrap -->
						</div><!-- .swipe -->
					</div><!-- .swiper-wrapper -->
				
				</div><!-- .sw -->
			</div>
			
			
			
			<div class="tooltip-selectors">
			
				<div class="tooltip-1">
					<span class="tooltip-content">
						<span class="h4-style">What is Low-E?</span>
						<p>A low-E, or low-emissivity, coating is a thin transparent metallic layer, only several atoms thick, 
						applied directly to the glazing surface. In conjunction with Argon gas, it increases the </p>
					</span>
				</div><!-- .tooltip-1 -->
				
				<div class="tooltip-2">
					<span class="tooltip-content">
						<span class="h4-style">What is brickmould?</span>
						<p>An exterior vinyl trim that is decorative, as well as efficient. Can come with built-in j-trim.</p>
					</span>
				</div><!-- .tooltip-2 -->
				
				<div class="tooltip-3">
					<span class="tooltip-content">
						<span class="h4-style">What is a drywall return?</span>
						<p>A piece of drywall that covers both an interior wall and the side or top perimeter of a window.</p>
					</span>
				</div><!-- .tooltip-3 -->
				
				<div class="tooltip-4">
					<span class="tooltip-content">
						<span class="h4-style">What is Egress?</span>
						<p>Egress code is the national building code for bedroom window requirements, which will allow means of escape in case of fire or emergency.</p>
					</span>
				</div><!-- .tooltip-4 -->
				
			</div><!-- .tooltip-selectors -->