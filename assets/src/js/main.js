//load all required scripts
requirejs(
	[
		'scripts/components/anchors.external.popup',
		'scripts/components/standard.accordion',
		'scripts/components/custom.select',
		'scripts/components/magnific.popup',
		'scripts/components/responsive.video',
		
		'scripts/components/tabs',
		'scripts/swipe.srf',
		'scripts/aspect.ratio',
		'scripts/lazy.images',
		'scripts/search',
		
		'scripts/product.nav',
		'scripts/build.price',
		
		//toggles a class of list-view at the width specified by collapse-at-###
		'scripts/blocks',

		'scripts/nav',
		'scripts/hero'
	],
	function() {		
		
		var $window = $(window);
		
		
		$(document)
			.on('click','.inline-video',function(e) {
				var 
					el = $(this),
					src = el.data('src');
					
				if(src.length && !el.hasClass('inline-video-playing')) {
					el
						.addClass('inline-video-playing')
						.append('<iframe src="'+src+'"/>');
				}
				
			})
			.on('tabChanged',function(e,el) {
			
				$(document).trigger('updateTemplate');
				
			});
		
		(function() {
			$('.video-container').each(function() {
				var 
					el = $(this),
					ratio = el.data('ratio');
					
					el.css({paddingBottom:ratio+'%'});
			});
		}());
		
		//tooltipster
		(function() {
		
			var tooltipsterMethods = {
				update: function() {
					$('.tooltipster')
						.filter(function() { return !$(this).hasClass('tooltipstered'); })
						.each(function() {
							var 
								el = $(this),
								opts = el.data();

							console.log(el[0]);
								
							el.tooltipster({
								content: $(opts.selector).html(),
								contentAsHTML:true
							});
					});
				}
			};
			
			tooltipsterMethods.update();
			$(document).on('updateTemplate.tooltipster',function() {
				tooltipsterMethods.update();
			});
		
		}());
		
});