define([
],function() {
		
	var 
		$body = $('body'),
		$document = $(document),
		search = {
		
			showClass: 'show-search',
			searchInput:null,
			
			getSearchInput: function() {
				if(!this.searchInput) {
					this.searchInput = $('form.search-form input');
				}
				
				return this.searchInput;
			},
			
			show: function() {
				$body.addClass(this.showClass);
				this.getSearchInput().val('').focus();
			},
			
			hide: function() {
				$body.removeClass(this.showClass);
				this.getSearchInput().blur();
			},
			
			toggle: function() {
				if(this.isShowing()) {
					this.hide();
				} else {
					this.show();
				}
			},
			
			isShowing: function() {
				return $body.hasClass(this.showClass);
			}
			
		}
		
	$('button.toggle-search-form')
		.on('click',function() {
			search.toggle();
		});
		
	$body
		.on('keyup',function(e) {
			if(search.isShowing() && e.keyCode === 27) {
				search.hide();
				return false;
			}
		})
		.on('click',function(e) { 
			search.isShowing() && $(e.target).hasClass('search-bg') && search.hide(); 
		});

});